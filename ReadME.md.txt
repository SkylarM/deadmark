## ABOUT

A wave based zombie survival shooter where the player fights through countless waves of enemies to reach the highest score and wave possible.

## CONTROLS

#### PC controls
WASD- Up, down, left, right
Mouse- Aim firearm
Shift- Move quicker
Left click- Shoot
Right click- Use powerup

#### Xbox Controller
Left stick- Up, down, left, right
Right stick- Aim firearm
L1- Move quicker
R2- Shoot
R1- Use powerup

#### Playstation Controller
Left stick- Up, down, left, right
Right stick- Aim firearm
LB- Move quicker
RT- Shoot
RB- Use powerup

## ENEMIES
- Zombies

## ITEMS
..* Health pickup
..* Ammo powerup

