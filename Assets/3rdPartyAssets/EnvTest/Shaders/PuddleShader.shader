﻿Shader "Custom/PuddleShader" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Normal ("Normal", 2D) = "bump" {}
		_Specular ("Specular", 2D) = "black" {}
		_Height ("Height", 2D) = "black" {}
		_FloodLevel ("Flood Level", Float) = 1
		_Contrast ("Blend Contrast", Float) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf StandardSpecular fullforwardshadows
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			fixed4 color : COLOR;
			float3 worldRefl;
			INTERNAL_DATA
		};

		half _Glossiness;
		half _Contrast;
		half _FloodLevel;
		sampler2D _Specular;
		sampler2D _Normal;
		sampler2D _Height;

		void surf (Input IN, inout SurfaceOutputStandardSpecular o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			float3 n = UnpackNormal(tex2D(_Normal, IN.uv_MainTex));
			float4 s = tex2D(_Specular, IN.uv_MainTex);
			float h = tex2D(_Height, IN.uv_MainTex).r;

			float wet = IN.color.r;
			wet = clamp(wet * wet * _Contrast + wet * _FloodLevel - h * h * _Contrast, 0, 1);

			o.Albedo = c.rgb * lerp(1, 0.5, wet);
			o.Normal = normalize(lerp(n, float3(0, 0, 1), wet * wet));
			o.Specular = lerp(s.rgb, 0.7, wet);
			o.Smoothness = lerp(s.a, 1, wet);
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
