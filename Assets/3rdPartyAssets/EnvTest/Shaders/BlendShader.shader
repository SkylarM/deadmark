﻿Shader "Custom/BlendShader" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Normal ("Normal", 2D) = "bump" {}
		_Specular ("Specular", 2D) = "black" {}
		_Height ("Height", 2D) = "black" {}

		_Albedo2 ("Albedo2 (RGB)", 2D) = "white" {}
		_Normal2 ("Normal2", 2D) = "bump" {}
		_Specular2 ("Specular2", 2D) = "black" {}
		_Height2 ("Height2", 2D) = "black" {}

		_Contrast ("Blend Contrast", Float) = 1

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf StandardSpecular fullforwardshadows
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float4 color : COLOR;
		};

		sampler2D _Normal;
		sampler2D _Specular;
		sampler2D _Height;
		sampler2D _Albedo2;
		sampler2D _Normal2;
		sampler2D _Specular2;
		sampler2D _Height2;

		float _Contrast;

		void surf (Input IN, inout SurfaceOutputStandardSpecular o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			float3 n = UnpackNormal(tex2D(_Normal, IN.uv_MainTex));
			float4 s = tex2D(_Specular, IN.uv_MainTex);
			float h = tex2D(_Height, IN.uv_MainTex).r;

			fixed4 c2 = tex2D(_Albedo2, IN.uv_MainTex);
			float3 n2 = UnpackNormal(tex2D(_Normal2, IN.uv_MainTex));
			float4 s2 = tex2D(_Specular2, IN.uv_MainTex);
			float h2 = tex2D(_Height2, IN.uv_MainTex).r;

			float blend = saturate(h2 * h2 + IN.color.r * _Contrast - h * h * _Contrast);

			o.Albedo = lerp(c.rgb, c2.rgb, blend);
			o.Normal = normalize(lerp(n.rgb, n2.rgb, blend));
			o.Specular = lerp(s.rgb, s2.rgb, blend);
			o.Smoothness = lerp(s.a, s2.a, blend);
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
