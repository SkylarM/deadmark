﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum WeaponsType {
		pistol,
		shotgun,
		m4
	}

public class GunScript : MonoBehaviour {

	public float damage = 10f;
	public float range = 100f;
	public float fireRate = 15f;
	float nextTimeToFire = 0f;
	public ParticleSystem muzzleFlash;
	public GameObject impactEffect;

	public AudioSource GunSounds;
	public AudioClip pistolSfx;
	public AudioClip shotgunSfx;

	public Camera fpsCam;

	public WeaponsType currentWeapon;
	Animator anim;

	void Awake () {
		anim = GetComponent<Animator>();
	}

	void Update () {

		if (currentWeapon == WeaponsType.pistol) {
			WeaponSystem.instance.pG.SetActive(true);
			WeaponSystem.instance.m4G.SetActive(false);
			WeaponSystem.instance.sG.SetActive(false);
			WeaponSystem.instance.pistolImage.SetActive(true);
			WeaponSystem.instance.m4image.SetActive(false);
			WeaponSystem.instance.shotgunImage.SetActive(false);
			if (WeaponSystem.instance.pistolAmmo >=1) {
				if (Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire) {
					nextTimeToFire = Time.time + 3.2f/fireRate;
					GunSounds.PlayOneShot(pistolSfx);
					anim.SetTrigger("pistolShoot");
					WeaponSystem.instance.pistolAmmo-=1;
					Shoot();
				}
			}
			if (WeaponSystem.instance.pistolAmmo <= 0) {
					if (Input.GetKeyDown(KeyCode.R)) {
						ReloadPistol();
					}
				}
		}

		if (currentWeapon == WeaponsType.m4) {
			WeaponSystem.instance.m4G.SetActive(true);
			WeaponSystem.instance.sG.SetActive(false);
			WeaponSystem.instance.pG.SetActive(false);
			WeaponSystem.instance.pistolImage.SetActive(false);
			WeaponSystem.instance.m4image.SetActive(true);
			WeaponSystem.instance.shotgunImage.SetActive(false);
			if (WeaponSystem.instance.m4Ammo >=1) {
				if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire) {
					nextTimeToFire = Time.time + 1f/fireRate;
					anim.SetTrigger("m4Shoot");
					GunSounds.PlayOneShot(pistolSfx);
					WeaponSystem.instance.m4Ammo-=1;
					Shoot();
				}
			}
			if (WeaponSystem.instance.m4Ammo <= 0) {
				if (Input.GetKeyDown(KeyCode.R)) {
						Reloadm4();
				}
			}
		}

		if (currentWeapon == WeaponsType.shotgun) {
			WeaponSystem.instance.sG.SetActive(true);
			WeaponSystem.instance.m4G.SetActive(false);
			WeaponSystem.instance.pG.SetActive(false);
			WeaponSystem.instance.pistolImage.SetActive(false);
			WeaponSystem.instance.m4image.SetActive(false);
			WeaponSystem.instance.shotgunImage.SetActive(true);
			if (WeaponSystem.instance.shotgunAmmo >=1) {
				if (Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire) {
					nextTimeToFire = Time.time + 10f/fireRate;
					GunSounds.PlayOneShot(shotgunSfx);
					WeaponSystem.instance.shotgunAmmo-=1;
					Shoot();
				}
			}
		}
		if (WeaponSystem.instance.shotgunAmmo <= 0) {
			if (Input.GetKeyDown(KeyCode.R)) {
				ReloadShotgun();
			}
		}
	}

	void Shoot() {
		RaycastHit hit;
		muzzleFlash.Play();
		if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range)) {

			HealthController health = hit.transform.GetComponent<HealthController>();

			if (health != null) {
				health.TakeDamage(damage);
			}

			GameObject impact = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
			Destroy(impact, 2f);
		}
	}

	public void ReloadPistol() {
		WeaponSystem.instance.pistolAmmo+=10;
	}

	public void Reloadm4() {
		WeaponSystem.instance.m4Ammo+=25;
	}

	public void ReloadShotgun() {
		WeaponSystem.instance.shotgunAmmo+=9;
	}
}
