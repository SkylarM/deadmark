﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRefSetter : MonoBehaviour {

	public Camera cam;

	void Awake () {
		CameraRefManager.SetMainCameraRef(cam);
	}
}
