﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelInteractableScript : Interactable {

    public GameObject movepoint;

    public override void Highlight()
    {
        highlight.SetActive(true);
    }
    public override void Lowlight()
    {
        highlight.SetActive(false);
    }
    public override void Interact()
    {
        base.Interact();
        PlayerMovement.instance.gameObject.transform.position = movepoint.transform.position;
    }
}
