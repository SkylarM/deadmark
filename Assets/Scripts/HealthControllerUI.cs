﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthControllerUI : MonoBehaviour {

	public Slider slider;
	void Awake () {
		slider = GetComponent<Slider>();
	}

	bool needsRefresh = true;
	void Update () {
		if (needsRefresh && PlayerMovement.instance != null) {
			PlayerMovement.instance.GetComponent<HealthController>().onHealthChanged += OnHealthChanged;
			needsRefresh = false;
		}
		else if (!needsRefresh && PlayerMovement.instance == null) {
			needsRefresh = true;
		}
	}

	void OnHealthChanged (float oldHealth, float health) {
		slider.value = health;
	}
}

