﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicGenerator : MonoBehaviour {

	private AudioSource audioSource;
    public AudioClip[] clips;

    void Start () {
        audioSource = FindObjectOfType<AudioSource>();
        audioSource.loop = false;
        
    }

	private AudioClip GetRandomClip() {
		return clips[Random.Range(0, clips.Length)];
	}
 
 	void Update () {
        if (!audioSource.isPlaying) {
            audioSource.clip = GetRandomClip();
            audioSource.Play();
        }
     }
}
