﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunInteractable : Interactable {

    public GameObject movepoint;

    public override void Highlight()
    {
        highlight.SetActive(true);
    }
    public override void Lowlight()
    {
        highlight.SetActive(false);
    }
    public override void Interact()
    {
        //base.Interact();
        //action here
    }
}
