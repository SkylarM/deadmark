﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausedScript : MonoBehaviour {

	public static PausedScript instance;

	public RectTransform pauseMenuRoot;
	public RectTransform controls;

	public bool isPaused;

	void Awake () {
		if (instance == null) instance = this;
		ResumeGame();
		controls.gameObject.SetActive(false);
	}

	public void PauseGame () {
		isPaused = true;
		pauseMenuRoot.gameObject.SetActive(true);
		Time.timeScale = 0;
		Cursor.visible = true;
	}

	public void ResumeGame () {
		isPaused = false;
		pauseMenuRoot.gameObject.SetActive(false);
		Time.timeScale = 1;
		PlayerMovement.instance.cursorLock = false;
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
	}

	public void QuitGame () {
		SceneManager.LoadSceneAsync(0);
	}

	public void Controls() {
		controls.gameObject.SetActive(true);
	}

	public void Back() {
		controls.gameObject.SetActive(false);
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape) && PlayerMovement.instance.canPause) {
			PauseGame();
		}
	}

	public void BackToMenu() {
		SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
	}
}
