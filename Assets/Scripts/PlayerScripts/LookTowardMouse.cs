﻿using UnityEngine;
using System.Collections;
 
public class LookTowardMouse : MonoBehaviour {

	public Transform player;
	Vector3 mouse;
	Vector3 dir;
	Camera cam;
	public Transform gunObject;

	void Start () {
		cam = CameraRefManager.mainCamera;
		Cursor.visible = false;
	}

	void Update() {
		/*if (cam == null) {
			Debug.Log("CameraNull");
		}
		Vector3 world = cam.ScreenToWorldPoint(Input.mousePosition);
		debugger.transform.position = world;
		dir = world - player.position;
		transform.right = dir; */
		
			RaycastHit hit;
			Ray ray = CameraRefManager.mainCamera.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit, float.MaxValue)) {
				if (hit.transform.gameObject.tag == "ground") {
					
					Vector3 hitPos = hit.point;
					hitPos.y = gunObject.position.y;
					dir = hitPos - player.position;
					transform.forward = dir;


				}
				else if (hit.transform.gameObject.tag == "Enemy") {
					Vector3 hitPos = hit.point;
					dir = hitPos - player.position;
					transform.forward = dir;
				}
			}
		}
	}
