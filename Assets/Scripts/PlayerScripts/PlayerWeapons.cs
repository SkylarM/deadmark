﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapons : MonoBehaviour {

	public static PlayerWeapons instance;

	[Header("These are the guns available to the player")]
	public GameObject m4;
	public GameObject RailGun;
	public GameObject Pistol;
	public GameObject ClusterBomb;
	public GameObject BeamGun;
	public GameObject M79GrenadeLauncher;
	
	[Header("This is where the gun is placed")]
	public GameObject weaponPlacement;

	public enum Weapon {
        M4,
		RailGun,
		Pistol,
		ClusterBomb,
		BeamGun,
		M79GrenadeLauncher
    }

	//public Weapon currentWeapon = Weapon.Pistol;

	void Awake () {
        if (instance == null) instance = this;
        ShowPistol();
    }

	void Update () {
       /* if (Input.GetKeyDown("space")) {
            switch (currentWeapon) {
                case Weapon.M4:
                    ShowM4();
                    break;
				case Weapon.RailGun:
					ShowRailGun();
					break;
				case Weapon.Pistol:
					ShowPistol();
					break;
				case Weapon.ClusterBomb:
					ShowClusterBomb();
					break;
				case Weapon.BeamGun:
					ShowBeamGun();
					break;
				case Weapon.M79GrenadeLauncher:
					ShowM79GrenadeLauncher();
					break;
                default:
                    break;
            }
        }*/
		if (Input.GetKeyDown("1")) {
			ShowPistol();
		}
		if (Input.GetKeyDown("2")) {
			ShowM4();
		}
		if (Input.GetKeyDown("3")) {
			ShowRailGun();
		}
		if (Input.GetKeyDown("4")) {
			ShowClusterBomb();
		}
		if (Input.GetKeyDown("5")) {
			ShowBeamGun();
		}
		if (Input.GetKeyDown("6")) {
			ShowM79GrenadeLauncher();
		}


		weaponPlacement.transform.position = weaponPlacement.transform.position;
    }


	public void ShowPistol() {
		//currentWeapon = Weapon.Pistol;
		Pistol.transform.position = weaponPlacement.transform.position;
		m4.gameObject.SetActive(false);
		RailGun.gameObject.SetActive(false);
		Pistol.gameObject.SetActive(true);
		ClusterBomb.gameObject.SetActive(false);
		BeamGun.gameObject.SetActive(false);
		M79GrenadeLauncher.gameObject.SetActive(false);
	}

	public void ShowM4() {
		//currentWeapon = Weapon.M4;
		m4.transform.position = weaponPlacement.transform.position;
		m4.gameObject.SetActive(true);
		RailGun.gameObject.SetActive(false);
		Pistol.gameObject.SetActive(false);
		ClusterBomb.gameObject.SetActive(false);
		BeamGun.gameObject.SetActive(false);
		M79GrenadeLauncher.gameObject.SetActive(false);
	}

	public void ShowRailGun() {
		//currentWeapon = Weapon.RailGun;
		RailGun.transform.position = weaponPlacement.transform.position;
		m4.gameObject.SetActive(false);
		RailGun.gameObject.SetActive(true);
		Pistol.gameObject.SetActive(false);
		ClusterBomb.gameObject.SetActive(false);
		BeamGun.gameObject.SetActive(false);
		M79GrenadeLauncher.gameObject.SetActive(false);
	}

	public void ShowClusterBomb() {
		//currentWeapon = Weapon.ClusterBomb;
		ClusterBomb.transform.position = weaponPlacement.transform.position;
		m4.gameObject.SetActive(false);
		RailGun.gameObject.SetActive(false);
		Pistol.gameObject.SetActive(false);
		ClusterBomb.gameObject.SetActive(true);
		BeamGun.gameObject.SetActive(false);
		M79GrenadeLauncher.gameObject.SetActive(false);
	}

	public void ShowBeamGun() {
		//currentWeapon = Weapon.BeamGun;
		BeamGun.transform.position = weaponPlacement.transform.position;
		m4.gameObject.SetActive(false);
		RailGun.gameObject.SetActive(false);
		Pistol.gameObject.SetActive(false);
		ClusterBomb.gameObject.SetActive(false);
		BeamGun.gameObject.SetActive(true);
		M79GrenadeLauncher.gameObject.SetActive(false);
	}

	public void ShowM79GrenadeLauncher() {
		//currentWeapon = Weapon.ClusterBomb;
		M79GrenadeLauncher.transform.position = weaponPlacement.transform.position;
		m4.gameObject.SetActive(false);
		RailGun.gameObject.SetActive(false);
		Pistol.gameObject.SetActive(false);
		ClusterBomb.gameObject.SetActive(false);
		BeamGun.gameObject.SetActive(false);
		M79GrenadeLauncher.gameObject.SetActive(true);
	}
}
