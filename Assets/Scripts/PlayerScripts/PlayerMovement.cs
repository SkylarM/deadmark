﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour {
	
	public static PlayerMovement instance;

	public float currentSpeed;
	[HideInInspector]public Transform cameraMain;
	public float jumpForce = 500;
	[HideInInspector]public Vector3 cameraPosition;

	[Header("Player SOUNDS")]
	public AudioSource _jumpSound;
	public AudioSource _reloading;
	public AudioSource _hitSound;
	public AudioSource _walkSound;
	public AudioSource _runSound;

	public GameObject ladderPlacementHands;
	GameObject ladder;
	GameObject ladderPlacementWorld;

	public GameObject deathCam;
	public GameObject deathScreen;
	public GameObject crosshair;
	public GameObject hud;
	public GameObject otherUI;

	public GameObject YouWinScreen;

	public float maxSpeed = 0.01f;
	public int runSpeed = 5;
	public float deaccelerationSpeed = 15.0f;
	public float accelerationSpeed = 50000.0f;
	public bool grounded;
	bool touchingLadder;
	bool nearLadderPlacementWorld  = false;
	public bool isRunning = false;
	private GameObject myBloodEffect;

	public bool canPause;

	private Vector3 slowdownV;
	private Vector2 horizontalMovement;

	public bool cursorLock;
	HealthController health;

	Rigidbody rb;

	void Awake() {
		if (instance == null) instance = this;
		rb = GetComponent<Rigidbody>();
		cameraMain = transform.Find("PlayersCamera").transform;
		ignoreLayer = 1 << LayerMask.NameToLayer ("Player");
		Cursor.visible = false;
		ladder = GameObject.FindGameObjectWithTag("MoveableLadder");
		ladderPlacementHands = GameObject.FindGameObjectWithTag("LadderPlacement");
		deathCam.SetActive(false);
		deathScreen.SetActive(false);
		cursorLock = true;
		Cursor.lockState = CursorLockMode.Locked;
		health = GetComponent<HealthController>();
		crosshair.SetActive(true);
		hud.SetActive(true);
		canPause = true;
		otherUI.SetActive(true);
		YouWinScreen.SetActive(false);
	}

	void OnEnable () {
		health.onHealthChanged += HealthChanged;
	}

	void OnDisable () {
		if (health) health.onHealthChanged -= HealthChanged;
	}

	void Update() {
		Jumping ();
		WalkingSound ();

		if (Input.GetKey(KeyCode.LeftShift)) {
			isRunning = true;
		}
		else {
			isRunning = false;
		}

		if (Input.GetKey(KeyCode.E) && touchingLadder) {
			ladder.transform.position = ladderPlacementHands.transform.position;
		}
		else if (Input.GetKeyUp(KeyCode.E) && nearLadderPlacementWorld) {
			ladder.transform.position = ladderPlacementWorld.transform.position;
		}

		if (cursorLock == true) {
			Cursor.lockState = CursorLockMode.Locked;
		}

		if (Input.GetKeyDown(KeyCode.Escape)) {
			cursorLock = false;
            Cursor.lockState = CursorLockMode.None;
		}
		
	}
	
	void FixedUpdate() {
		PlayersMovement ();
	}
	
	void PlayersMovement() {
		currentSpeed = rb.velocity.magnitude;
		horizontalMovement = new Vector2 (rb.velocity.x, rb.velocity.z);
			if (horizontalMovement.magnitude > maxSpeed){
				horizontalMovement = horizontalMovement.normalized;
				horizontalMovement *= maxSpeed;    
			}
			rb.velocity = new Vector3 (
				horizontalMovement.x,
				rb.velocity.y,
				horizontalMovement.y
			);
			if (grounded){
				rb.velocity = Vector3.SmoothDamp(rb.velocity,
					new Vector3(0,rb.velocity.y,0),
					ref slowdownV,
					deaccelerationSpeed);

		}

		if (isRunning) {
			rb.AddRelativeForce (Input.GetAxis("Horizontal") * accelerationSpeed * Time.deltaTime, 0, Input.GetAxis ("Vertical") * accelerationSpeed * Time.deltaTime);
		}

		if (grounded) {
			rb.AddRelativeForce (Input.GetAxis ("Horizontal") * accelerationSpeed * Time.deltaTime, 0, Input.GetAxis ("Vertical") * accelerationSpeed * Time.deltaTime);
		} else {
			rb.AddRelativeForce (Input.GetAxis ("Horizontal") * accelerationSpeed / 2 * Time.deltaTime, 0, Input.GetAxis ("Vertical") * accelerationSpeed / 2 * Time.deltaTime);

		}
		if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {
			deaccelerationSpeed = 0.5f;
		} else {
			deaccelerationSpeed = 0.1f;
		}
	}

	void Jumping() {
		if (Input.GetKeyDown(KeyCode.Space) && grounded) {
			rb.AddRelativeForce (Vector3.up * jumpForce);
			if (_jumpSound)
				_jumpSound.Play();
			else
			_walkSound.Stop ();
			_runSound.Stop ();
		}
	}

	void WalkingSound() {
		if (_walkSound && _runSound) {
			if (RayCastGrounded ()) {			
				if (currentSpeed > 1) {
					if (maxSpeed == 3) {
						if (!_walkSound.isPlaying) {
							_walkSound.Play ();
							_runSound.Stop ();
						}					
					} 
					else if (maxSpeed == 5) {
						if (!_runSound.isPlaying) {
							_walkSound.Stop ();
							_runSound.Play ();
						}
					}
				} else {
					_walkSound.Stop ();
					_runSound.Stop ();
				}
			} else {
				_walkSound.Stop ();
				_runSound.Stop ();
			}
		} 
	}
	
	bool RayCastGrounded() {
		RaycastHit groundedInfo;
		if(Physics.Raycast(transform.position, transform.up *-1f, out groundedInfo, 1, ~ignoreLayer)){
			Debug.DrawRay (transform.position, transform.up * -1f, Color.red, 0.0f);
			if(groundedInfo.transform != null){
				return true;
			}
			else{
				return false;
			}
		}
		return false;
	}

	void OnCollisionStay(Collision other) {
		foreach(ContactPoint contact in other.contacts){
			if(Vector2.Angle(contact.normal,Vector3.up) < 60){
				grounded = true;
			}
		}
	}
	
	void OnCollisionExit() {
		grounded = false;
	}

	void OnTriggerEnter(Collider c) {
		if (c.gameObject.CompareTag("MoveableLadder")) {
			touchingLadder = true;
			Debug.Log("Touching Ladder");
		}
		if (c.gameObject.CompareTag("LadderPlacement")) {
			nearLadderPlacementWorld = true;
			Debug.Log("Touching LadderPlacementInworld");
		}
	}

	public void Death () {
		deathCam.SetActive(true);
		deathScreen.SetActive(true);
		crosshair.SetActive(false);
		hud.SetActive(false);
		canPause = false;
		otherUI.SetActive(false);
		YouWinScreen.SetActive(false);
		Cursor.visible = true;
	}

	public void Win () {
		deathCam.SetActive(true);
		deathScreen.SetActive(false);
		crosshair.SetActive(false);
		hud.SetActive(false);
		canPause = false;
		otherUI.SetActive(false);
		YouWinScreen.SetActive(true);
		Cursor.visible = true;
	}

	void HealthChanged (float previousHealth, float health) {
		if (previousHealth > health) {
			if (health <= 0) {
                rb.velocity = Vector2.zero;
			}
			else if (previousHealth > health) {
			}
		}
	}

	[Header("Shooting Properties")]
	private LayerMask ignoreLayer;

	

}
