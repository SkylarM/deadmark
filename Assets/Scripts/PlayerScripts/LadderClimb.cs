﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderClimb : MonoBehaviour {

	public GameObject player;
	public bool inside = false;
	public float heightFactor = 3.2f;

	//PlayerMovement pm;

	void Start () {
		//pm = GetComponent<PlayerMovement>();
	}

	void Update() {
		if (inside == true && Input.GetKey("w")) {
			player.transform.position += Vector3.up / heightFactor;
		}
	}

	void OnTriggerStay(Collider c) {
		if (c.gameObject.CompareTag("Ladder")) {
			//pm.enabled = false;
			inside = true;
		}
	}

	void OnTriggerExit(Collider c) {
		if (c.gameObject.CompareTag("Ladder")) {
			//pm.enabled = true;
			inside = false;
		}
	}

	
}
