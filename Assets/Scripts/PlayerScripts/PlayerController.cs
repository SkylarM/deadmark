﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    
	/*public float speed = 10;    
   	public float turnSpeed = 10;
	public float boostSpeed = 150;
	public float normalSpeed = 90;
	bool isBoostSpeed = false;
	public float maxStamina = 100;
	public float stamina = 100;

	public Text scoreText;
    public int score = 0;
    public int bulletsRemain = 0;
    public Text bulletsText;

	public Slider healthSlider;
	public Slider staminaSlider;

	public int playerNum = 0;
	public string[] horizontalAxis = new string[] {"Horizontal", "Horizontal2"};
	public string[] verticalAxis = new string[] {"Vertical", "Vertical2"};
	public KeyCode[] fireButton = new KeyCode[] {KeyCode.Joystick1Button0, KeyCode.Joystick2Button0};
	public KeyCode[] altFireButton = new KeyCode[] {KeyCode.Space, KeyCode.KeypadEnter};
	public KeyCode[] scoreboardButton = new KeyCode[] {KeyCode.Joystick1Button3, KeyCode.Joystick2Button3};
	public KeyCode[] staminaButton = new KeyCode[] {KeyCode.Joystick1Button8, KeyCode.Joystick2Button8};
	public static Dictionary<int, PlayerController> players = new Dictionary<int, PlayerController>();
	
   	HealthController health;
	Rigidbody body;
	Animator anim;

	void Awake () 
    {
		players[playerNum] = this;
		anim = GetComponent<Animator>();
        Debug.Log("Player " + (playerNum+1).ToString());
		stamina = maxStamina;
		health = GetComponent<HealthController>();
    	body = GetComponent<Rigidbody>();
    }
    
    
	void OnEnable()
    {
		if (health) health.onHealthChanged += HealthChanged;
	}

	void OnDisable()
    {
		if (health) health.onHealthChanged -= HealthChanged;
	}
	
	void Update() 
	{

		scoreText.text = score + "";
        bulletsText.text = "Bullets: " + bulletsRemain;
		staminaSlider.value = stamina;
		
		if (health.health <= 0)
        {
			return;
		}
		
		float x = Input.GetAxis(horizontalAxis[playerNum]);
        float y = Input.GetAxis(verticalAxis[playerNum]);
        Vector3 dir = new Vector3(x, 0, y).normalized;
        body.velocity = dir * speed;

		if(dir.magnitude > 0.1f) {
        Vector3 cross = Vector3.Cross(transform.forward, dir);
        transform.Rotate(Vector3.up * cross.y * turnSpeed * Time.deltaTime);
		}
        //for the walking animation to play once you have a run key remove the block comment
       /* if(dir != Vector3.zero /*&& !Input.GetKeyDown(*run input variable here*)*/
      /*  {
            //change the animator's to walk here
        }

        else if(dir != Vector3.zero /*^^ Input.GetKeyDown(*run input variable here*)*/
        
            //change the animator to run here
        
		//anim.SetFloat("turnVelocity", body.angularVelocity.y);
		//anim.SetFloat("forwardVelocity", Vector3.Dot(body.velocity, transform.forward));

	/*	if (Input.GetKey(scoreboardButton[playerNum]))
		{
			GameManager.instance.scoreboard.SetActive(true);
		}
		else 
		{
			GameManager.instance.scoreboard.SetActive(false);
		}

		if(Input.GetKey(staminaButton[playerNum]) && stamina > 0) 
		{
			speed = boostSpeed;
			isBoostSpeed = true;
			staminaSlider.value = stamina;
			stamina--;
		}
		else 
		{
			speed = normalSpeed;
			isBoostSpeed = false;
		}

		if (health.enemyKilled) {
			score += 50;
			stamina+=10;
		}
	}

	void HealthChanged(float previousHealth, float health)
    {
		healthSlider.value = health;

		if (previousHealth > health)
        {
			if (health <= 0)
            {
                //DeathScreen
			}
		}
	}

	public void KilledEnemy()
    {
        score += 50;
        scoreText.text = score + "";
    } */
}

    
