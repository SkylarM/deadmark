﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {

    public GameObject highlight;

    private void Start()
    {
        highlight.SetActive(false);
    }
    public virtual void Highlight()
    {
        Debug.Log("DO A THING");
    }
    public virtual void Lowlight()
    {
        Debug.Log("DO A THING");
    }
    public virtual void Interact()
    {
        Debug.Log("DO A THING");
    }
}
