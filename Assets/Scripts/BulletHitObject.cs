﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHitObject : MonoBehaviour {

	public float damage = 1;
	public bool isBullet;

	void OnCollisionEnter(Collision c)  {
        HitObject(c.gameObject);

		if (c.gameObject.CompareTag("Environment")) {
			gameObject.SetActive(false);
		}
    }

	void OnTriggerEnter2D (Collider2D c) {
        if (c.gameObject.CompareTag("Player") && isBullet) {
            gameObject.SetActive(false);
        }
        HitObject(c.gameObject);
    }

    void HitObject (GameObject g) {
        HealthController health = g.GetComponent<HealthController>();
        if (health != null) {
            health.TakeDamage(damage);
        }
    }
}
