﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour {

	public GameObject loadingScreen;
	public static LoadingScreen instance;
	private GameObject levelSelect;
	public Slider loadingBar;
	
	void Awake () {
		if (instance == null) {
			DontDestroyOnLoad(gameObject);
			instance = this;
			loadingScreen.SetActive(false);
		}
		else {
			Destroy(gameObject);
		}
	}

	public static void LoadScene (int num) {
		instance.StartCoroutine(instance.Loading(num));
		MainMenuManager.instance.fileSelectScreen.gameObject.SetActive(false);
	}

	IEnumerator Loading (int num) {
		loadingScreen.SetActive(true);
		yield return new WaitForSeconds(4f);
		AsyncOperation a = SceneManager.LoadSceneAsync(num);

		while (!a.isDone) {
			loadingBar.value = a.progress;
			yield return new WaitForEndOfFrame();
		}

		loadingScreen.SetActive(false);
	}
}
