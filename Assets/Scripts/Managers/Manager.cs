﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    protected bool _disableBecauseBroken = false;

    #region VIRTUAL MONO METHODS
    void Awake()
    {
        OnAwake();
        CheckVars();
    }
    protected virtual void OnAwake() { }

    void Start()
    {
        OnStart();
    }
    protected virtual void OnStart() { }

    void Update()
    {
        OnUpdate();
    }
    protected virtual void OnUpdate() { }

    void FixedUpdate()
    {
        OnFixedUpdate();
    }
    protected virtual void OnFixedUpdate() { }
    #endregion

    #region ADDITIONAL VIRTUAL METHODS
    protected virtual void CheckVars()
    {
        _disableBecauseBroken = false;
    }
    #endregion
}
