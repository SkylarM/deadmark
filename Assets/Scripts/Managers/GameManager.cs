﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    public GameObject YouWinScreen;

    public AudioSource source;
    public AudioClip zombieScreach;
    public AudioClip letsgetthisover;
	public AudioClip putrid;


    public Text zombieCountText;
    public int zombieCount = 0;
    public int bulletsRemain = 0;

	public Slider healthSlider;


    HealthController health;

    void Awake()
    {
        if (instance == null) instance = this;
        zombieCount = 30;
        zombieCountText.text = "Zombies Remain: " + zombieCount;
        //source = GetComponent<AudioSource>();
        StartCoroutine(MeteorShowerVoice());

    }

    void OnEnable()
    {
		if (health) health.onHealthChanged += HealthChanged;
	}

	void OnDisable()
    {
		if (health) health.onHealthChanged -= HealthChanged;
	}


    void Update () 
    {
        zombieCountText.text = "Zombies Remain: " + zombieCount;

        if (zombieCount <= 0) {
            PlayerMovement.instance.Win();
        }
        if (zombieCount == 20) {
            source.PlayOneShot(zombieScreach);
        }
    }

    void HealthChanged(float previousHealth, float health)
    {
		healthSlider.value = health;

		if (previousHealth > health)
        {
			if (health <= 0)
            {
                //DeathScreen
			}
		}
	}

    public void KilledEnemy()
    {
        zombieCount -= 1;
        zombieCountText.text = "Zombies Remain: " + zombieCount;
    }

    IEnumerator MeteorShowerVoice () {
        yield return new WaitForSeconds(4);
        source.PlayOneShot(letsgetthisover);
        yield return new WaitForSeconds(20);
        source.PlayOneShot(putrid);
    }

}
