﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour {

	public string[] messages;

	void OnTriggerEnter (Collider c) {
		if (c.CompareTag("Player")) {
			TextDisplayController.Show(messages);
		}
	}

	void OnTriggerExit (Collider c) {
		if (c.CompareTag("Player")) {
			TextDisplayController.Hide();
		}
	}
}
