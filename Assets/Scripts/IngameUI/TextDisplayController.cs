﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDisplayController : MonoBehaviour {

	public static TextDisplayController instance;

	public GameObject textBox;
	public Text displayText;

	public enum State {
		Hidden,
		Prompting,
		Displaying,
		Done
	}

	public State state = State.Hidden;

	private string[] messages;
	private int currentMessage;

	void Awake () {
		if (instance == null) {
			instance = this;
		}
	}

	void Update () {
		switch (state) {
			case State.Hidden:
			textBox.SetActive(false);
			break;
		case State.Prompting:
			PromptUpdate();
			break;
		case State.Displaying:
			DisplayUpdate();
			break;
		case State.Done:
			DoneUpdate();
			break;
		default:
			Debug.LogWarning("Shouldnt be here");
			break;
		}
	}

	public static void Show (string[] messages) {
		instance.ShowPrompt();
		instance.messages = messages;

	}

	public static void Hide () {
		instance.state = State.Hidden;

	}

	void ShowPrompt () {
		state = State.Prompting;
		currentMessage = 0;
		textBox.SetActive(true);
	}

	void PromptUpdate () {
		if (Input.GetKeyDown(KeyCode.JoystickButton3)) {
			state = State.Displaying;
			textBox.SetActive(true);
		}
	}

	void DisplayUpdate () {
		displayText.text = messages[currentMessage];
		if (Input.GetKeyDown(KeyCode.JoystickButton3)) {
			currentMessage++;
			if (currentMessage == messages.Length-1) {
				state = State.Done;
				displayText.text = messages[currentMessage];
			}
		}
	}

	void DoneUpdate () {
		if (Input.GetKeyDown(KeyCode.JoystickButton3)) {
			ShowPrompt();
		}
	}
}
