﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponSystem : MonoBehaviour {

	public static WeaponSystem instance;

	public GameObject[] weapons;
	public int startingWeaponIndex = 0;	
	private int weaponIndex;

	public int pistolAmmo = 16;
	public int m4Ammo = 25;
	public int shotgunAmmo = 9;

	public GameObject pG;
	public GameObject m4G;
	public GameObject sG;
	public Text pistolText;
	public Text m4Text;
	public Text shotgunText;	

	public GameObject pistolImage;
	public GameObject m4image;
	public GameObject shotgunImage;				

	void Awake() {
		if (instance == null) instance = this;
		pistolImage.SetActive(true);
		m4image.SetActive(false);
		shotgunImage.SetActive(false);
	}

	void Start() {
		weaponIndex = startingWeaponIndex;
		SetActiveWeapon(weaponIndex);

		pG.SetActive(false);
		m4G.SetActive(false);
		sG.SetActive(false);
	}

	void Update() {

		pistolText.text = pistolAmmo + "/16";
		m4Text.text = m4Ammo + "/225";
		shotgunText.text = shotgunAmmo + "/30";

		if (Input.GetButtonDown("Weapon 1"))
			SetActiveWeapon(0);
		if (Input.GetButtonDown("Weapon 2"))
			SetActiveWeapon(1);
		if (Input.GetButtonDown("Weapon 3"))
			SetActiveWeapon(2);
		if (Input.GetAxis("Mouse ScrollWheel") > 0)
			NextWeapon();
		if (Input.GetAxis("Mouse ScrollWheel") < 0)
			PreviousWeapon();
	}

	public void SetActiveWeapon(int index) {

		weaponIndex = index;

		for (int i = 0; i < weapons.Length; i++) {
			weapons[i].SetActive(false);
		}

		weapons[index].SetActive(true);
	}

	public void NextWeapon() {
		weaponIndex++;
		if (weaponIndex > weapons.Length - 1)
			weaponIndex = 0;
		SetActiveWeapon(weaponIndex);
	}

	public void PreviousWeapon() {
		weaponIndex--;
		if (weaponIndex < 0)
			weaponIndex = weapons.Length - 1;
		SetActiveWeapon(weaponIndex);
	}
}
