﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FileSetActive : MonoBehaviour {

	public GameObject saveFile1;
	public GameObject saveFile2;
	public GameObject saveFile3;

	void Awake() {
		saveFile1.SetActive(false);
		saveFile2.SetActive(false);
		saveFile3.SetActive(false);
	}

	public void SaveFile1 () {
		saveFile1.SetActive(true);
		saveFile2.SetActive(false);
		saveFile3.SetActive(false);
	}

	public void SaveFile2 () {
		saveFile1.SetActive(false);
		saveFile2.SetActive(true);
		saveFile3.SetActive(false);
	}

	public void SaveFile3 () {
		saveFile1.SetActive(false);
		saveFile2.SetActive(false);
		saveFile3.SetActive(true);
	}
}
