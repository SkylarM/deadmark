﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterButtonController : MonoBehaviour {

	public string letter;
	public Text displayLetter;

	void OnEnable () {
		if (letter == "A") GetComponent<Selectable>().Select();
	}

	public void SetupWithLetter (string letter) {
		this.letter = letter;
		displayLetter.text = letter;
	}

	public void ButtonClicked () {
		NameEntryController.AddLetter(letter);
	}
}	
