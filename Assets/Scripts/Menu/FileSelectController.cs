﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FileSelectController : MonoBehaviour {

	public string zombieFormat = "Zombies Killed: ";
	public string highScoreFormat = "Highscore: ";
	public string levelFormat = "Level: ";
    public Text nameText;
    public Text levelText;
    public Text zombiesKilledText;
    public Text highScoreText;


    void OnEnable () {
        if (SaveFileExists()) {
            SaveManager.SaveFile saveFile = SaveManager.Read(transform.GetSiblingIndex());
            nameText.text = saveFile.name;
            zombiesKilledText.text = string.Format(zombieFormat, saveFile.health, saveFile.maxHealth, saveFile.scene);
            highScoreText.text = string.Format(highScoreFormat, saveFile.health, saveFile.maxHealth, saveFile.scene);
            levelText.text = string.Format(levelFormat, saveFile.health, saveFile.maxHealth, saveFile.scene);
        }
        else {
            nameText.text = "Welcome Back!";
            levelText.text = "Level: ";
            highScoreText.text = "Highscore: ";
            zombiesKilledText.text = "Total Zombies Killed: ";
        }
    }

	bool SaveFileExists () {
		int fileID = transform.GetSiblingIndex();
		return (SaveManager.Read(fileID) != null);
	}

	public void ButtonClicked () {
		if (SaveFileExists()) {
			MainMenuManager.instance.ShowFileSelectScreen();
		}
		else {
			MainMenuManager.instance.ShowSignInScreen();
			NameEntryController.instance.fileID = transform.GetSiblingIndex();
		}
	}


	public void ConfirmDelete () {
		SaveManager.Delete(transform.GetSiblingIndex());
		nameText.text = "New Game";
		levelText.text = "Level: ";
		highScoreText.text = "Highscore: ";
		zombiesKilledText.text = "Total Zombies Killed: ";
	}
}
