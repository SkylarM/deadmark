﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour {

	public static MainMenuManager instance;

    public bool gameStarted = false;

    public RectTransform titleScreen;
	public RectTransform selectScreen;
	public RectTransform creditScreen;
    public RectTransform optionsScreen;
	public RectTransform instructionsScreen;
    public RectTransform signInScreen;
    public RectTransform fileSelectScreen;
    
    public Selectable optionsSelectable;
	public Selectable instructionsSelectable;
    public Selectable creditScreenSelectable;
    public Selectable signInScreenSelectable;
    
    public enum Screen {
        Title,
		Select,
		Instructions,
        Credit,
        Options,
        SignIn,
        FileSelect,
        Back
    }

    public Screen currentScreen = Screen.Title;

    void Awake () {
        if (instance == null) instance = this;
        ShowTitleScreen();
    }

    void Update () {
        if (Input.GetKeyDown("space")) {
            switch (currentScreen) {
                case Screen.Title:
                    ShowTitleScreen();
                    break;
				case Screen.Select:
                    ShowSelectScreen();
                    break;
				case Screen.Instructions:
                    ShowInstructionsScreen();
					break;
				case Screen.Credit:
                    ShowCreditScreen();
                    break;
                case Screen.Options:
                    ShowOptionsScreen();
                    break;
                case Screen.SignIn:
                    ShowSignInScreen();
                    break;
                case Screen.FileSelect:
                    ShowFileSelectScreen();
                    break;
                default:
                    break;
            }
        }
    }

	public void ShowTitleScreen () {
		currentScreen = Screen.Title;
		//EventSystem.current.SetSelectedGameObject(null);
		titleScreen.gameObject.SetActive(true);
		instructionsScreen.gameObject.SetActive(false);
		selectScreen.gameObject.SetActive(false);
        creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(false);
        signInScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(false);
	}

	public void ShowSelectScreen () {
        currentScreen = Screen.Select;
        EventSystem.current.SetSelectedGameObject(null);
		titleScreen.gameObject.SetActive(false);
		selectScreen.gameObject.SetActive(true);
        creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(false);
        signInScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(false);
    }

   public void ShowInstructionsScreen () {
	    currentScreen = Screen.Instructions;
		EventSystem.current.SetSelectedGameObject(null);
		instructionsScreen.gameObject.SetActive(true);
		titleScreen.gameObject.SetActive(false);
		creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(false);
        signInScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(false);
        selectScreen.gameObject.SetActive(false);
   }

    public void ShowCreditScreen () {
        currentScreen = Screen.Credit;
		EventSystem.current.SetSelectedGameObject(null);
		instructionsScreen.gameObject.SetActive(false);
		titleScreen.gameObject.SetActive(false);
		creditScreen.gameObject.SetActive(true);
        optionsScreen.gameObject.SetActive(false);
        signInScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(false);
        selectScreen.gameObject.SetActive(false);
	}

    public void ShowOptionsScreen () {
        currentScreen = Screen.Options;
        EventSystem.current.SetSelectedGameObject(null);
		instructionsScreen.gameObject.SetActive(false);
		titleScreen.gameObject.SetActive(false);
		creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(true);
        signInScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(false);
        selectScreen.gameObject.SetActive(false);
    }

    public void GoBack () {
        currentScreen = Screen.Select;
        EventSystem.current.SetSelectedGameObject(null);
		instructionsScreen.gameObject.SetActive(false);
		titleScreen.gameObject.SetActive(false);
        selectScreen.gameObject.SetActive(true);
        creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(false);
        signInScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(false);
    }

    public void ShowSignInScreen () {
		currentScreen = Screen.Title;
		EventSystem.current.SetSelectedGameObject(null);
		titleScreen.gameObject.SetActive(false);
		instructionsScreen.gameObject.SetActive(false);
		selectScreen.gameObject.SetActive(false);
        creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(false);
        signInScreen.gameObject.SetActive(true);
        fileSelectScreen.gameObject.SetActive(false);
	}

    public void ShowFileSelectScreen () {
		currentScreen = Screen.Title;
		EventSystem.current.SetSelectedGameObject(null);
		titleScreen.gameObject.SetActive(false);
		instructionsScreen.gameObject.SetActive(false);
		selectScreen.gameObject.SetActive(false);
        creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(false);
        signInScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(true);
	}

    public void QuitGame () {
        Application.Quit();
    }

    public void StartGame (int num) {
        gameStarted = true;
        LoadingScreen.LoadScene(num);

    }

    private FileSelectController fileSelect;
	public GameObject deleteFilePrompt;
	public void PromptDelete (FileSelectController fileSelect) {
		this.fileSelect = fileSelect;
		deleteFilePrompt.SetActive(true);
	}

	public void ConfirmDelete (bool confirm) {
		deleteFilePrompt.SetActive(false);
		if (confirm) {
			fileSelect.ConfirmDelete();
		}
	}
}
