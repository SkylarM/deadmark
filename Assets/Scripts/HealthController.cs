﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour {

	public delegate void OnHealthChanged (float previousHealth, float health);
	public event OnHealthChanged onHealthChanged = delegate {};

	
	public float maxHealth = 3;
	public float health;
	public Slider healthSlider;

	public AudioSource source;
	public AudioClip grunt;

	public float oldHealth;
	public bool isPlayer;
	public bool isEnemy;
	public bool enemyKilled;
	bool canChange;
	bool isDead;

	void Awake () {
		//player = GetComponent<PlayerController>();
		health = maxHealth;
	}

	public void TakeDamage (float damage) {
		canChange = true;
		if (isPlayer) healthSlider.value = health;
		float oldHealth = health;
		if (isPlayer) source.PlayOneShot(grunt);
		
		if (canChange) {
			health -= damage;
			health = Mathf.Clamp(health, 0, maxHealth);
			onHealthChanged(oldHealth, health);
		}

		if (isPlayer && health <= 0) {
			PlayerMovement.instance.Death();
		}

		health -= damage;
		if (health <= 0) {
			isDead = true;
		}
	}

	public void AddHealth (float h) {
			health += h;
			health = Mathf.Clamp(health, 0, maxHealth);
			onHealthChanged(oldHealth, health);
	}
}

