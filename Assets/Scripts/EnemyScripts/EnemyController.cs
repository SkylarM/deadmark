﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    CapsuleCollider collider;
    NavMeshAgent agent;
    public float speed = 2;
    public float chaseDist = 5;
    public float turnSpeed = 1;
    public float attackDist = 2;

    bool isDead = false;
    

    public enum State
    {
        Idle,
        Chase,
        Attack,
        Dead
    }

    public State state = State.Idle;

    public GameObject hitboxLeft;
    public GameObject hitboxRight;

    Animator anim;
    GameObject player;
    Rigidbody body;
    HealthController health;

    void Awake() {
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        health = GetComponent<HealthController>();
        collider = GetComponent<CapsuleCollider>();
    }

    void OnEnable() {
		health.onHealthChanged += HealthChanged;
	}

	void OnDisable() {
		health.onHealthChanged -= HealthChanged;
	}

    void Start()
    {
        player = PlayerMovement.instance.gameObject;
        agent = GetComponent<NavMeshAgent>();
        agent.destination = player.transform.position;
    }

    void Update() {
        switch (state) {
            case State.Idle:
                IdleUpdate();
                break;
            case State.Chase:
                ChaseUpdate();
                break;
            case State.Attack:
                StartCoroutine(Attack());
                break;
            case State.Dead:
                DeadUpdate();
                break;
            default:
                break;
        }
    }

    void IdleUpdate()
    {
        anim.SetBool("isWalking", false);
        body.velocity = Vector3.zero;
        float dist = Vector3.Distance(transform.position, player.transform.position);
        if (dist < chaseDist)
        {
            state = State.Chase;
        }
    }

    void ChaseUpdate()
    {
        Vector3 dir = (player.transform.position - transform.position).normalized;
        Vector3 cross = Vector3.Cross(transform.forward, dir);
        transform.Rotate(Vector3.up * cross.y * turnSpeed * Time.deltaTime);

        float dist = Vector3.Distance(player.transform.position, transform.position);
        if (dist > chaseDist)
        {
            state = State.Idle;

        }
        else if (dist < attackDist)
        {
            state = State.Attack;
            body.velocity = Vector3.zero;
        }
        else
        {
            anim.SetBool("isWalking", true);
            agent.destination = player.transform.position;
        }
    }

    void DeadUpdate()
    {
        agent.destination = agent.transform.position;
        body.velocity = Vector3.zero;
    }

    IEnumerator Attack()
    {
        anim.SetBool("isWalking", false);
        anim.SetBool("shouldAttack", true);
        yield return new WaitForSeconds(3f);
        float dist = Vector3.Distance(transform.position, player.transform.position);
        if (dist > attackDist)
        {
            anim.SetBool("shouldAttack", false);
            state = State.Chase;
        }
    }

    void ActivateTrigger()
    {
        hitboxLeft.SetActive(true);
        hitboxRight.SetActive(true);
    }

    void EndAttack()
    {
        hitboxLeft.SetActive(false);
        hitboxRight.SetActive(false);
    }

    public void HealthChanged (float maxHealth, float health) {
		if (health <= 0 && !isDead)
        {
            collider.enabled = !collider.enabled;
            Debug.Log("thing");
			state = State.Dead;
            isDead = true;
            anim.SetTrigger("dead");
            GameManager.instance.KilledEnemy();
        }
		else
        {
			anim.SetTrigger("hit");
		}
	}
}

